# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1]
### Added
- `overseer` that forwards a terminal application to port 80
### Modified
- Request the use of `cartographer` SLAM instead of `amcl` to provide `navigation2` with SLAM

## [0.1.0]
### Added
- ROS2 `turtlebot3` bringup
- Patch of `LDS-01` to publish /scan under reliable topic
- `navigation2_client` that requests `navigation2` with amcl from the server
