# Turtle

![ROS](https://img.shields.io/badge/ROS-dashing-orange.svg)

The **Turtle** repository is a part of *Aalborg University - Robotics 7* project *Fleet Management of Distributed Multi-Robot Systems for Autonomous Data Collection* that coexists with services contained within [**Server**](https://gitlab.com/rob768/project/server) repository. This repository consists of services that are executed on a mobile robot.

> Note: This repository was tested with three [*TurtleBot3 Burger*](http://www.robotis.us/turtlebot-3/) mobile robots with `armv8` (`aarch64`) architecture. However, the implementation is platform and architecture agnostic, so one can substitute *TurtleBot3 Burger* specifics with a different robot.


## Services

This repository contains three services that are grouped under a single [`docker-compose`](docker-compose.yml).

- [**turtlebot3**](turtlebot3_ros2_patched) - Provides basic robot functionalities, i.e. interfacing of sensors and actuators, mobile drive control and robot state publisher.
- [**navigation2_client**](navigation2_client) - Requests **Server** to start `navigation2` with SLAM (`cartographer`) for a specific `ROS_DOMAIN_ID` based on robot's `HOSTNAME`.
- [**overseer**](overseer) - Forwards CLI application to *port 80* (using [*GoTTY*](https://github.com/yudai/gotty)). If utilised with *balenaCloud*, this service provides low-level control and overview of a robot through public URL.

> Note: Only service `navigation2_client` requires `navigation2_slam_server` to be accessible via a local area network.


## Dependencies

Dependencies for this repository are based on the deployment method. 
- If utilising [*balenaCloud*](https://www.balena.io/cloud/), follow [this](https://github.com/balena-io/balena-cli/blob/master/INSTALL.md) guide.
- If building locally, follow guides for [Docker Engine](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/).
- Or use any platform that supports `Docker` containers.


## Building, Deployment And Running

If utilising [*balenaCloud*](https://www.balena.io/cloud/), follow these steps:
```sh
$ git clone https://gitlab.com/rob768/project/turtle.git && cd turtle
$ balena push <application>
```

If building locally, follow these steps:
```sh
$ git clone https://gitlab.com/rob768/project/turtle.git && cd turtle
# docker-compose up
```

## Versioning

This repository utilises Semantic Versioning, please see [CHANGELOG](CHANGELOG.md) for more information.


## License

[MIT License](LICENSE)
