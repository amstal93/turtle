// Copyright 2019 ROB768

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "navigation2_slam_server/srv/start_navigation2.hpp"
#include "navigation2_slam_server/srv/stop_navigation2.hpp"
#include "rclcpp/rclcpp.hpp"

using namespace std::placeholders;
using StartNavigation2Srv = navigation2_slam_server::srv::StartNavigation2;
using StopNavigation2Srv = navigation2_slam_server::srv::StopNavigation2;

const std::string NODE_NAME = "navigation2_client";
const std::string SERVER_SRV_NAME_START = "navigation2_slam_server/start";
const std::string SERVER_SRV_NAME_STOP = "navigation2_slam_server/stop";
const bool STOP_IF_START_FAILED = true;

class Navigation2Outsourced : public rclcpp::Node {
  /* Properties */
 private:
  std::shared_ptr<rclcpp::Client<StartNavigation2Srv>> srv_client_start_;
  inline static std::shared_ptr<rclcpp::Client<StopNavigation2Srv>>
      srv_client_stop_;

  /* Methods */
 public:
  Navigation2Outsourced();

 private:
  static void signal_handler(int signal);
  void request_navigation2_start();
};

Navigation2Outsourced::Navigation2Outsourced() : Node(NODE_NAME) {
  // Register signal
  std::signal(SIGTERM, Navigation2Outsourced::signal_handler);

  // Create service clients
  srv_client_start_ =
      this->create_client<StartNavigation2Srv>(SERVER_SRV_NAME_START);
  srv_client_stop_ =
      this->create_client<StopNavigation2Srv>(SERVER_SRV_NAME_STOP);

  // Wait until services are available
  while (!(srv_client_start_->wait_for_service(std::chrono::seconds(1)) &&
           srv_client_stop_->wait_for_service(std::chrono::seconds(1)))) {
    if (!rclcpp::ok()) {
      RCLCPP_ERROR(this->get_logger(),
                   "Client interrupted while waiting for services to appear");
      exit(1);
    }
    RCLCPP_INFO(this->get_logger(), "Waiting for services to appear");
  }

  // Request start of `navigation2`
  request_navigation2_start();
}

void Navigation2Outsourced::request_navigation2_start() {
  // Create request for starting `navigation2`
  std::shared_ptr<StartNavigation2Srv::Request> request =
      std::make_shared<StartNavigation2Srv::Request>();

  // Get `ROS_DOMAIN_ID` based on `HOSTNAME`, while keeping only digits
  std::string hostname_ros_domain_id = std::getenv("HOSTNAME");
  hostname_ros_domain_id.erase(
      std::remove_if(hostname_ros_domain_id.begin(),
                     hostname_ros_domain_id.end(),
                     [](char c) { return !std::isdigit(c); }),
      hostname_ros_domain_id.end());

  // Fill in the request
  request->ros_domain_id = std::stoi(hostname_ros_domain_id);

  RCLCPP_INFO(this->get_logger(), "Calling `%s` with `ROS_DOMAIN_ID=%d`",
              SERVER_SRV_NAME_START.c_str(), request->ros_domain_id);

  // Create response callback for the service
  auto response_received_callback =
      [this](rclcpp::Client<StartNavigation2Srv>::SharedFuture future) {
        std::shared_ptr<StartNavigation2Srv::Response> result = future.get();
        if (result) {
          RCLCPP_INFO(this->get_logger(), "Calling `%s` succeded",
                      SERVER_SRV_NAME_START.c_str());
        } else {
          RCLCPP_ERROR(this->get_logger(), "Calling `%s` failed",
                       SERVER_SRV_NAME_START.c_str());
          if (STOP_IF_START_FAILED) signal_handler(SIGTERM);
          exit(1);
        }
      };

  // Call the service
  auto result_future = srv_client_start_->async_send_request(
      request, response_received_callback);
}

void Navigation2Outsourced::signal_handler(int signal) {
  std::cout << "Navigation2Outsourced::signal_handler(" << signal << ")\n";
  // Create request for stopping `navigation2`
  std::shared_ptr<StopNavigation2Srv::Request> request =
      std::make_shared<StopNavigation2Srv::Request>();

  // Get `ROS_DOMAIN_ID` based on `HOSTNAME`, while keeping only digits
  std::string hostname_ros_domain_id = std::getenv("HOSTNAME");
  hostname_ros_domain_id.erase(
      std::remove_if(hostname_ros_domain_id.begin(),
                     hostname_ros_domain_id.end(),
                     [](char c) { return !std::isdigit(c); }),
      hostname_ros_domain_id.end());

  // Fill `ROS_DOMAIN_ID` into the request
  request->ros_domain_id = std::stoi(hostname_ros_domain_id);

  // Call the service
  srv_client_stop_->async_send_request(request);

  // Here we do not care about reply as there is not much we can do.
}

int main(int argc, char* argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Navigation2Outsourced>());
  rclcpp::shutdown();
  return 0;
}
