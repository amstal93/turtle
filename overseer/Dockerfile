# Define what ROS2 distribution to use
ARG ROS_DISTRO=dashing

# Define the ROS2 parent image with the given distribution
FROM ros:${ROS_DISTRO}

# Install `Gotty`
RUN apt-get update \
    && apt-get install -y \
        gcc make \
        golang-1.10 \
    && rm -rf /var/lib/apt/lists/* \
    && export PATH=/usr/lib/go-1.10/bin:${PATH} \
    && go get github.com/yudai/gotty

# Expose port 80 for `Gotty`
EXPOSE 80

# Define the name of the current package
ENV PACKAGE_NAME=overseer
# Create workdir for current package
WORKDIR /${PACKAGE_NAME}

# Copy everything to the container
COPY . ./

# Define default ROS2 DDS domain
ENV ROS_DOMAIN_ID=232

# Determine whether to make the `ROS_DOMAIN_ID` based on `HOSTNAME`
ENV ROS_DOMAIN_ID_BASED_ON_HOSTNAME=false

# Execute the script
CMD ["/bin/bash", "/overseer/start.sh"]
